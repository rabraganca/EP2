# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

Para compilar, utilizar e testar o programa, faca o seguinte:
1 - baixe o projeto para sua maquina.
2 - abra o net beans
2 - File->New Project
3 - Java-> Java Project with Existing Resources
4 - Browse (escolha o local do projeto na sua maquina) -> next.

Para compilar e rodar o projeto, basta abrir o projeto no lateral de projetos. Source Packages->Controller->main.java , clicar com o botao direito e escolher a opcao Run File. Tambem e possivel rodar utilizando os atalhos shift+f6.
