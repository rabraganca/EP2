
package model;

import controller.TabControl;

public class Employee extends Person {
    private String password;
    
    
    public Employee(String nome,String password){
        super(nome);
        this.password = password;
    }

    public void setPassword(String password){
        this.password = password;
    }
    
    public String getPassword() {
        return password;
    }
    
 
}
