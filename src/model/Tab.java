
package model;

import java.util.ArrayList;
import model.Client;
import model.Employee;
import model.Product;

public class Tab {
    private ArrayList<Product> products;
    private Client client;
    private Employee employee;
    private String tableNumber;
    private String comments;
    private String paymentMethod;
    private Float totalPayment;
    
      public Tab(Client client, Employee employee, String tablenumber){
        this.products = new ArrayList<Product>();
        this.client = client;
        this.employee = employee;
        this.tableNumber = tablenumber;
        this.comments = comments;
        this.paymentMethod = paymentMethod;
        this.totalPayment = totalPayment;
    }

    public ArrayList<Product> getTab() {
        return products;
    }
    
    public void add(Product product){
        products.add(product);
    }
    
    public void remove(Product product){
        products.remove(product);
    }
    
    public Product search(String code) {
        for (Product b: products) {
            if (b.getName().equalsIgnoreCase(code)) return b;
        }
        return null;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setTablenumber(String tablenumber) {
        this.tableNumber = tablenumber;
    }

    public String getTablenumber() {
        return tableNumber;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setTotalPayment(Float totalPayment) {
        this.totalPayment = totalPayment;
    }

    public Float getTotalPayment() {
        return totalPayment;
    }
   
    
    
}
