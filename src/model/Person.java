
package model;

public class Person {
    private String nome;
    
    public Person(String nome){
        this.nome = nome;
    }

    public String getName() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
    
}
