
package model;


public class Product {
    private String name;
    private Integer quantity;
    private Float price;
    private String status;
    private Integer lowCount;
    
    public Product(String name, Integer quantity, Float price , Integer lowCount){
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.lowCount = lowCount;
        setStatus();
        
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public Integer getQuantity() {
        return quantity;
    }
    
    public void setPrice(Float price){
        this.price = price;
    }
    
    public Float getPrice(){
        return price;
    }
    
    public void setStatus(){
        if(getQuantity() > getLowCount()){
            this.status = "OK";
        }else if(getQuantity() <= getLowCount()){
            this.status = "LOW";
        }
    }
    
    public boolean isStatusValid(){
       if(getQuantity() <= getLowCount()){
           return false;
       }else{
           return true;
       }
    }
    
    public String getStatus(){
        return status;
    }
    
    public void setLowCount(Integer lowCount){
        this.lowCount = lowCount;
    }
    
    public Integer getLowCount(){
        return lowCount;
    }
    
    public void Minus(){
        if(quantity>0){
            this.quantity = quantity-1;
        }
    }
   
     
     
}
