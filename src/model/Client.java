
package model;

public class Client extends Person{
    private String name;
    private String cpf;
    
    
    public Client(String name,String cpf){
        super(name);
        this.cpf = cpf;
    }
    
    public void setCpf(String cpf){
        this.cpf = cpf;
    }
    
    public String getCpf(){
        return cpf;
    }
    
  
}
