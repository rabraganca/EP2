
package model;


public class Manager {
    private Employee employee;
    
    
    public Manager(Employee employee){
        this.employee = employee;
    }
    
    public Employee getEmployee(){
        return employee;
    }
    
    
}
