
package controller;

import model.Employee;

/**
 * @author rabrg
 */
public class main {
    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
        mainFrame.setDefaultCloseOperation(MainFrame.DISPOSE_ON_CLOSE);
    }
    
}
