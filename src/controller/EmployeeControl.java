
package controller;

import java.util.ArrayList;
import model.Employee;

public class EmployeeControl {
    private ArrayList<Employee> employeeList;
    
    public EmployeeControl(){
        this.employeeList = new ArrayList<Employee>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }
    
    public void add(Employee employee){
        employeeList.add(employee);
    }
    
    public void remove(Employee employee){
        employeeList.remove(employee);
    }
    
    public Employee search(String password) {
        for (Employee b: employeeList) {
            if (b.getPassword().equalsIgnoreCase(password)) return b;
        }
        return null;
    }
    
}
