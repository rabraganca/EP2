
package controller;

import java.util.ArrayList;
import java.util.Collection;
import model.Dish;
import model.Product;


public class ProductControl {
    private ArrayList<Product> productList;
    private ArrayList<Product> productListLow;
    
    public ProductControl(){
        this.productList = new ArrayList<Product>();
        this.productListLow = new ArrayList<Product>();
    }
            
    
    public ArrayList<Product> getProductList() {
        return productList;
    }
    
    public void add(Product product){
        productList.add(product);
    }
    
    public void remove(Product product){
        productList.remove(product);
    }
    
    public Product search(String name) {
        for (Product product: productList) {
            if (product.getName().equalsIgnoreCase(name)) return product;
        }
        return null;
    }

     public ArrayList<Product> getProductListLow() {
        return productListLow;
    }
    
    public void addLow(Product product){
        productListLow.add(product);
    }
    
    public void removeLow(Product product){
        productListLow.remove(product);
    }
            
}
