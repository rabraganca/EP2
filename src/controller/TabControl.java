
package controller;

import model.Tab;
import java.util.ArrayList;


public class TabControl {
        private ArrayList<Tab> tabList;
        private ArrayList<Tab> closedTabList;
    public TabControl(){
        this.tabList= new ArrayList<Tab>();
        this.closedTabList = new ArrayList<Tab>();
    }

    public ArrayList<Tab> getTabList() {
        return tabList;
    }
    
    public void add(Tab tab){
        tabList.add(tab);
    }
    
    public void remove(Tab tab){
        tabList.remove(tab);
    }
    
    public Tab search(String name) {
        for (Tab b: tabList) {
            if (b.getClient().getName().equalsIgnoreCase(name)) return b;
        }
        return null;
    }
    
    public ArrayList<Tab> getClosedTabList() {
        return closedTabList;
    }
    
    public void addClosed(Tab tab){
        closedTabList.add(tab);
    }
    
    public void removeClosed(Tab tab){
        closedTabList.remove(tab);
    }
}
