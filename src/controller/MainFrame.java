
package controller;


import model.Tab;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.Client;
import model.Dessert;
import model.Drink;
import model.Employee;
import model.Dish;
import model.Manager;
import model.Product;


public class MainFrame extends javax.swing.JFrame {

    private EmployeeControl employeeList;
    private ProductControl productList;
    private TabControl tabList;
    private Employee employee;
    private Dish food;
    private Drink drink;
    private Dessert dessert;
    private Manager manager;
    private Client client;
    private Tab tab;
    private String name;
    private String password;
    private DefaultTableModel table;
    
    public MainFrame() {
        initComponents();
        defaultManager();
        this.employeeList = new EmployeeControl();
        this.productList = new ProductControl();
        this.tabList= new TabControl();
        this.table = (DefaultTableModel) jTableTab.getModel();
        panelStock.setVisible(false);
        panelLogin.setVisible(true);
        
        
       
    }
    

    private void defaultManager(){
        Employee defaultEmployee = new Employee("rafael","12");
        Manager manager = new Manager(defaultEmployee);
        this.manager = manager;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        panelStock = new javax.swing.JPanel();
        jComboBoxStock = new javax.swing.JComboBox<>();
        btnLowCount = new javax.swing.JButton();
        lblProductStock = new javax.swing.JLabel();
        stockScrollPane = new javax.swing.JScrollPane();
        stockTable = new javax.swing.JTable();
        stockAddPanel = new javax.swing.JPanel();
        lblAddProduct = new javax.swing.JLabel();
        txtAddProduct = new javax.swing.JTextField();
        lblAddQuantity = new javax.swing.JLabel();
        txtAddQuantity = new javax.swing.JTextField();
        lblAddPrice = new javax.swing.JLabel();
        txtAddPrice = new javax.swing.JTextField();
        btnAddToStock = new javax.swing.JButton();
        btnDone = new javax.swing.JButton();
        lblAddLowCount = new javax.swing.JLabel();
        txtAddLowCount = new javax.swing.JTextField();
        btnListStock = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTableRecords = new javax.swing.JTable();
        btnRecordsList = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        panelLogin = new javax.swing.JPanel();
        panelLoginData = new javax.swing.JPanel();
        txtLoginManagerUser = new javax.swing.JTextField();
        txtLoginManagerPassword = new javax.swing.JTextField();
        lblUser = new javax.swing.JLabel();
        lblPassword = new javax.swing.JLabel();
        btnLoginSignin = new javax.swing.JButton();
        lblPicture = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        panelNewEmployee = new javax.swing.JPanel();
        lblNewEmployeeName = new javax.swing.JLabel();
        lblNewEmployeePassword = new javax.swing.JLabel();
        txtNewEmployeeName = new javax.swing.JTextField();
        txtNewEmployeePassword = new javax.swing.JTextField();
        lblManagerPassword = new javax.swing.JLabel();
        txtManagerPassword = new javax.swing.JTextField();
        btnConfirmNewEmployee = new javax.swing.JButton();
        panelMain = new javax.swing.JPanel();
        lblEmployeeDisplay = new javax.swing.JLabel();
        lblEmployeeNameDisplay = new javax.swing.JLabel();
        lblManagerDisplay = new javax.swing.JLabel();
        lblManagerNameDisplay = new javax.swing.JLabel();
        btnManagerLogout = new javax.swing.JButton();
        btnEmployeeLogout = new javax.swing.JButton();
        btnStock = new javax.swing.JButton();
        btnRefreshTable = new javax.swing.JButton();
        jTabbedDishes = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableDishes = new javax.swing.JTable();
        jTabbedDrinks = new javax.swing.JTabbedPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableDrinks = new javax.swing.JTable();
        jTabbedDesserts = new javax.swing.JTabbedPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTableDesserts = new javax.swing.JTable();
        panelNewTab = new javax.swing.JPanel();
        lblNewClientName = new javax.swing.JLabel();
        lblNewTableNumber = new javax.swing.JLabel();
        txtNewTabClientName = new javax.swing.JTextField();
        txtNewTabTableNumber = new javax.swing.JTextField();
        btnNewTable = new javax.swing.JButton();
        lblNewClientCpf = new javax.swing.JLabel();
        txtNewTabClientCpf = new javax.swing.JTextField();
        panelTab = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableTab = new javax.swing.JTable();
        jComboBoxTabs = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        btnAddDish = new javax.swing.JButton();
        btnAddDrinks = new javax.swing.JButton();
        btnAddDessert = new javax.swing.JButton();
        btnRemoveProduct = new javax.swing.JButton();
        btnConfirmProducts = new javax.swing.JButton();
        panelTabProducts = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableCheckout = new javax.swing.JTable();
        lblTotal = new javax.swing.JLabel();
        lblTotalPriceTab = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtComments = new javax.swing.JTextArea();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtCommentsTab = new javax.swing.JTextArea();
        btnCloseTab = new javax.swing.JButton();
        rbDebit = new javax.swing.JRadioButton();
        rbCredit = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Restaurant \"Tô com fome, quero mais\"");

        jComboBoxStock.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Food", "Drink", "Dessert" }));
        jComboBoxStock.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxStockItemStateChanged(evt);
            }
        });
        jComboBoxStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxStockActionPerformed(evt);
            }
        });

        btnLowCount.setText("Low Count");
        btnLowCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLowCountActionPerformed(evt);
            }
        });

        lblProductStock.setText("Product");

        stockTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product", "Quantity", "Price", "Status"
            }
        ));
        stockScrollPane.setViewportView(stockTable);

        stockAddPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblAddProduct.setText("Product");

        txtAddProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddProductActionPerformed(evt);
            }
        });

        lblAddQuantity.setText("Quantity");

        txtAddQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAddQuantityActionPerformed(evt);
            }
        });

        lblAddPrice.setText("Price");

        btnAddToStock.setText("Add");
        btnAddToStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddToStockActionPerformed(evt);
            }
        });

        btnDone.setText("Back");
        btnDone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDoneActionPerformed(evt);
            }
        });

        lblAddLowCount.setText("Low Cunt");

        javax.swing.GroupLayout stockAddPanelLayout = new javax.swing.GroupLayout(stockAddPanel);
        stockAddPanel.setLayout(stockAddPanelLayout);
        stockAddPanelLayout.setHorizontalGroup(
            stockAddPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txtAddProduct, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(txtAddQuantity)
            .addComponent(txtAddPrice)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, stockAddPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAddProduct)
                .addGap(24, 24, 24))
            .addComponent(txtAddLowCount)
            .addGroup(stockAddPanelLayout.createSequentialGroup()
                .addGroup(stockAddPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(stockAddPanelLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(lblAddQuantity))
                    .addGroup(stockAddPanelLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(stockAddPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(stockAddPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnAddToStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnDone, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE))
                            .addComponent(lblAddLowCount)))
                    .addGroup(stockAddPanelLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(lblAddPrice)))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        stockAddPanelLayout.setVerticalGroup(
            stockAddPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(stockAddPanelLayout.createSequentialGroup()
                .addComponent(lblAddProduct)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddProduct, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAddQuantity)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblAddPrice)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(lblAddLowCount)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtAddLowCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(btnAddToStock)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnDone, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnListStock.setText("List");
        btnListStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListStockActionPerformed(evt);
            }
        });

        jTableRecords.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Client", "Payment Method", "Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane8.setViewportView(jTableRecords);

        btnRecordsList.setText("List");
        btnRecordsList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecordsListActionPerformed(evt);
            }
        });

        jLabel2.setText("Records");

        javax.swing.GroupLayout panelStockLayout = new javax.swing.GroupLayout(panelStock);
        panelStock.setLayout(panelStockLayout);
        panelStockLayout.setHorizontalGroup(
            panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStockLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelStockLayout.createSequentialGroup()
                        .addComponent(lblProductStock)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBoxStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnListStock, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(btnLowCount))
                    .addComponent(stockScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(stockAddPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 85, Short.MAX_VALUE)
                .addGroup(panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelStockLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRecordsList, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        panelStockLayout.setVerticalGroup(
            panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelStockLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelStockLayout.createSequentialGroup()
                        .addGroup(panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBoxStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnLowCount)
                            .addComponent(lblProductStock)
                            .addComponent(btnListStock))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stockScrollPane))
                    .addComponent(stockAddPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelStockLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panelStockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnRecordsList)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        panelLogin.setBorder(javax.swing.BorderFactory.createTitledBorder("Manager Login"));
        panelLogin.setToolTipText("");

        panelLoginData.setBorder(javax.swing.BorderFactory.createTitledBorder("Login"));

        txtLoginManagerUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLoginManagerUserActionPerformed(evt);
            }
        });

        txtLoginManagerPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLoginManagerPasswordActionPerformed(evt);
            }
        });

        lblUser.setText("User");

        lblPassword.setText("Password");

        btnLoginSignin.setText("Sign in");
        btnLoginSignin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginSigninActionPerformed(evt);
            }
        });

        lblPicture.setIcon(new javax.swing.ImageIcon("/home/rabrg/Documents/20162/oo/EP2/Icons/userLogin.png")); // NOI18N

        jLabel3.setText("User: rafael");

        jLabel4.setText("Password: 12");

        javax.swing.GroupLayout panelLoginDataLayout = new javax.swing.GroupLayout(panelLoginData);
        panelLoginData.setLayout(panelLoginDataLayout);
        panelLoginDataLayout.setHorizontalGroup(
            panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLoginDataLayout.createSequentialGroup()
                .addContainerGap(327, Short.MAX_VALUE)
                .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(btnLoginSignin, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(47, 47, 47))
            .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelLoginDataLayout.createSequentialGroup()
                    .addGap(43, 43, 43)
                    .addComponent(lblPicture)
                    .addGap(32, 32, 32)
                    .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblPassword)
                        .addComponent(lblUser))
                    .addGap(53, 53, 53)
                    .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtLoginManagerUser)
                        .addComponent(txtLoginManagerPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(43, Short.MAX_VALUE)))
        );
        panelLoginDataLayout.setVerticalGroup(
            panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLoginDataLayout.createSequentialGroup()
                .addGap(163, 163, 163)
                .addComponent(btnLoginSignin)
                .addGap(41, 41, 41)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelLoginDataLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(panelLoginDataLayout.createSequentialGroup()
                            .addGap(53, 53, 53)
                            .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtLoginManagerUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblUser))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(panelLoginDataLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtLoginManagerPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblPassword)))
                        .addComponent(lblPicture, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(245, Short.MAX_VALUE)))
        );

        panelNewEmployee.setBorder(javax.swing.BorderFactory.createTitledBorder("Register a new Employee"));

        lblNewEmployeeName.setText("Name:");

        lblNewEmployeePassword.setText("Password:");

        lblManagerPassword.setText("Manager Password");

        btnConfirmNewEmployee.setText("Confirm");
        btnConfirmNewEmployee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmNewEmployeeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelNewEmployeeLayout = new javax.swing.GroupLayout(panelNewEmployee);
        panelNewEmployee.setLayout(panelNewEmployeeLayout);
        panelNewEmployeeLayout.setHorizontalGroup(
            panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNewEmployeeLayout.createSequentialGroup()
                .addGroup(panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelNewEmployeeLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnConfirmNewEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelNewEmployeeLayout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelNewEmployeeLayout.createSequentialGroup()
                                .addComponent(lblManagerPassword)
                                .addGap(18, 66, Short.MAX_VALUE)
                                .addComponent(txtManagerPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelNewEmployeeLayout.createSequentialGroup()
                                .addComponent(lblNewEmployeeName)
                                .addGap(18, 18, 18)
                                .addComponent(txtNewEmployeeName))
                            .addGroup(panelNewEmployeeLayout.createSequentialGroup()
                                .addComponent(lblNewEmployeePassword)
                                .addGap(18, 18, 18)
                                .addComponent(txtNewEmployeePassword)))))
                .addGap(167, 167, 167))
        );
        panelNewEmployeeLayout.setVerticalGroup(
            panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNewEmployeeLayout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNewEmployeeName)
                    .addComponent(txtNewEmployeeName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNewEmployeePassword)
                    .addComponent(txtNewEmployeePassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelNewEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblManagerPassword)
                    .addComponent(txtManagerPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addComponent(btnConfirmNewEmployee)
                .addContainerGap(183, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelLoginLayout = new javax.swing.GroupLayout(panelLogin);
        panelLogin.setLayout(panelLoginLayout);
        panelLoginLayout.setHorizontalGroup(
            panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelLoginLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLoginData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelNewEmployee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelLoginLayout.setVerticalGroup(
            panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLoginLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(panelLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelNewEmployee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelLoginData, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(128, Short.MAX_VALUE))
        );

        panelMain.setVisible(false);

        lblEmployeeDisplay.setText("Employee:");

        lblManagerDisplay.setText("Manager:");

        btnManagerLogout.setText("Logout");
        btnManagerLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManagerLogoutActionPerformed(evt);
            }
        });

        btnEmployeeLogout.setText("Logout");
        btnEmployeeLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmployeeLogoutActionPerformed(evt);
            }
        });

        btnStock.setText("Stock");
        btnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStockActionPerformed(evt);
            }
        });

        btnRefreshTable.setText("Refresh");
        btnRefreshTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshTableActionPerformed(evt);
            }
        });

        jTabbedDishes.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTableDishes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dish", "Quantity", "Price", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableDishes.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTableDishes);
        jTableDishes.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTabbedDishes.addTab("Dishes", jScrollPane1);

        jTabbedDrinks.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTableDrinks.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drinks", "Quantity", "Price", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableDrinks.setColumnSelectionAllowed(true);
        jScrollPane3.setViewportView(jTableDrinks);
        jTableDrinks.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTabbedDrinks.addTab("Drinks", jScrollPane3);

        jTabbedDesserts.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jTableDesserts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dessert", "Quantity", "Price", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableDesserts.setColumnSelectionAllowed(true);
        jScrollPane4.setViewportView(jTableDesserts);
        jTableDesserts.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jTabbedDesserts.addTab("Desserts", jScrollPane4);

        panelNewTab.setBorder(javax.swing.BorderFactory.createTitledBorder("New Client"));

        lblNewClientName.setText("Client Name:");

        lblNewTableNumber.setText("Table Number:");

        txtNewTabTableNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNewTabTableNumberActionPerformed(evt);
            }
        });

        btnNewTable.setText("New Table");
        btnNewTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewTableActionPerformed(evt);
            }
        });

        lblNewClientCpf.setText("Client Cpf:");

        txtNewTabClientCpf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNewTabClientCpfActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelNewTabLayout = new javax.swing.GroupLayout(panelNewTab);
        panelNewTab.setLayout(panelNewTabLayout);
        panelNewTabLayout.setHorizontalGroup(
            panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNewTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNewClientName)
                    .addComponent(lblNewClientCpf)
                    .addComponent(lblNewTableNumber))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNewTabTableNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNewTabClientName, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNewTabClientCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnNewTable, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
        );
        panelNewTabLayout.setVerticalGroup(
            panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelNewTabLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelNewTabLayout.createSequentialGroup()
                        .addGroup(panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNewClientName)
                            .addComponent(txtNewTabClientName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNewClientCpf)
                            .addComponent(txtNewTabClientCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelNewTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNewTableNumber)
                            .addComponent(txtNewTabTableNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(btnNewTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );

        panelTab.setBorder(javax.swing.BorderFactory.createTitledBorder("Add Products"));

        jTableTab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product", "Quantity", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(jTableTab);

        javax.swing.GroupLayout panelTabLayout = new javax.swing.GroupLayout(panelTab);
        panelTab.setLayout(panelTabLayout);
        panelTabLayout.setHorizontalGroup(
            panelTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTabLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(165, 165, 165))
        );
        panelTabLayout.setVerticalGroup(
            panelTabLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelTabLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(188, 188, 188))
        );

        jComboBoxTabs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxTabsActionPerformed(evt);
            }
        });

        jLabel1.setText("TABS");

        btnAddDish.setText("Add");
        btnAddDish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDishActionPerformed(evt);
            }
        });

        btnAddDrinks.setText("Add");
        btnAddDrinks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDrinksActionPerformed(evt);
            }
        });

        btnAddDessert.setText("Add");
        btnAddDessert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddDessertActionPerformed(evt);
            }
        });

        btnRemoveProduct.setText("Remove");
        btnRemoveProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveProductActionPerformed(evt);
            }
        });

        btnConfirmProducts.setText("Confirm Products");
        btnConfirmProducts.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmProductsActionPerformed(evt);
            }
        });

        panelTabProducts.setBorder(javax.swing.BorderFactory.createTitledBorder("Client Tab"));

        jTableCheckout.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Products", "Quantity"
            }
        ));
        jScrollPane2.setViewportView(jTableCheckout);

        lblTotal.setText("Total:");

        javax.swing.GroupLayout panelTabProductsLayout = new javax.swing.GroupLayout(panelTabProducts);
        panelTabProducts.setLayout(panelTabProductsLayout);
        panelTabProductsLayout.setHorizontalGroup(
            panelTabProductsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTabProductsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelTabProductsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                    .addGroup(panelTabProductsLayout.createSequentialGroup()
                        .addComponent(lblTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalPriceTab)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelTabProductsLayout.setVerticalGroup(
            panelTabProductsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelTabProductsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelTabProductsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotal)
                    .addComponent(lblTotalPriceTab)))
        );

        txtComments.setColumns(20);
        txtComments.setRows(5);
        jScrollPane7.setViewportView(txtComments);

        txtCommentsTab.setEditable(false);
        txtCommentsTab.setColumns(20);
        txtCommentsTab.setRows(5);
        jScrollPane6.setViewportView(txtCommentsTab);

        btnCloseTab.setText("Close Tab");
        btnCloseTab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseTabActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbDebit);
        rbDebit.setText("Debit");

        buttonGroup1.add(rbCredit);
        rbCredit.setText("Credit");

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnRefreshTable, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelMainLayout.createSequentialGroup()
                                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelMainLayout.createSequentialGroup()
                                        .addComponent(lblManagerDisplay)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lblManagerNameDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelMainLayout.createSequentialGroup()
                                        .addComponent(lblEmployeeDisplay)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblEmployeeNameDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(68, 68, 68)
                                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnEmployeeLogout)
                                    .addComponent(btnManagerLogout)))))
                    .addComponent(jTabbedDishes)
                    .addComponent(jTabbedDrinks)
                    .addComponent(jTabbedDesserts))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(btnAddDish)
                        .addGap(24, 24, 24)
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelMainLayout.createSequentialGroup()
                                .addComponent(panelNewTab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panelMainLayout.createSequentialGroup()
                                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMainLayout.createSequentialGroup()
                                        .addGap(0, 7, Short.MAX_VALUE)
                                        .addComponent(panelTab, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelMainLayout.createSequentialGroup()
                                        .addComponent(btnRemoveProduct, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnConfirmProducts)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(jScrollPane7))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(panelTabProducts, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelMainLayout.createSequentialGroup()
                                            .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jComboBoxTabs, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGap(18, 18, 18)
                                            .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(panelMainLayout.createSequentialGroup()
                                                    .addComponent(rbCredit)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(rbDebit))
                                                .addComponent(btnCloseTab, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMainLayout.createSequentialGroup()
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddDrinks)
                            .addComponent(btnAddDessert))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMainLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(btnManagerLogout))
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblManagerNameDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblManagerDisplay))))
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lblEmployeeNameDisplay, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblEmployeeDisplay))
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(btnEmployeeLogout)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRefreshTable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnAddDish, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)
                    .addComponent(jTabbedDishes, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedDrinks, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddDrinks, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(btnAddDessert, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(24, 24, 24))
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addComponent(jTabbedDesserts, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnStock, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelNewTab, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnCloseTab))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBoxTabs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnConfirmProducts)
                        .addComponent(btnRemoveProduct)
                        .addComponent(rbCredit)
                        .addComponent(rbDebit)))
                .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelMainLayout.createSequentialGroup()
                        .addGroup(panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelTab, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(panelTabProducts, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(8, 8, 8)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelStock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtLoginManagerUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLoginManagerUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLoginManagerUserActionPerformed

    private void btnLoginSigninActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginSigninActionPerformed
        try{
            name = txtLoginManagerUser.getText();
            password = txtLoginManagerPassword.getText();
            
            for(Employee e : employeeList.getEmployeeList()){
                    if(e.getName().equals(name) && e.getPassword().equals(password)){
                        employee = e;
                        
                        txtLoginManagerUser.setText("");
                        txtLoginManagerPassword.setText("");

                        panelLogin.setVisible(false);
                        panelMain.setVisible(true);
                        

                        lblManagerNameDisplay.setText(manager.getEmployee().getName());
                        lblEmployeeNameDisplay.setText(employee.getName());
                        password  = "";
                    }else{
                        JOptionPane.showMessageDialog(rootPane, "Acesso negado!");
                    }
            }
            
                
            
        }catch(IndexOutOfBoundsException e){
            
        }catch(NullPointerException e){
        
        }
        
            
    }//GEN-LAST:event_btnLoginSigninActionPerformed

    private void txtLoginManagerPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLoginManagerPasswordActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLoginManagerPasswordActionPerformed

    private void btnEmployeeLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmployeeLogoutActionPerformed
        lblEmployeeNameDisplay.setText("");
        panelMain.setVisible(false);
        panelLogin.setVisible(true);
    }//GEN-LAST:event_btnEmployeeLogoutActionPerformed

    private void btnManagerLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManagerLogoutActionPerformed
        panelMain.setVisible(false);
        panelLogin.setVisible(true);
    }//GEN-LAST:event_btnManagerLogoutActionPerformed

    private void txtAddProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddProductActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAddProductActionPerformed

    private void txtAddQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAddQuantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAddQuantityActionPerformed

    private void btnAddToStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddToStockActionPerformed
        try{
            String name;
            float price;
            Integer quantity,lowCount;
            name = txtAddProduct.getText();
            quantity = Integer.parseInt(txtAddQuantity.getText());
            price = Float.parseFloat(txtAddPrice.getText());
            lowCount = Integer.parseInt(txtAddLowCount.getText());

            if(jComboBoxStock.getSelectedItem().toString().equals("Food")){
                Dish food = new Dish(name,quantity,price,lowCount);
                productList.add(food);
            }else if(jComboBoxStock.getSelectedItem().toString().equals("Drink")){
                Drink drink = new Drink(name,quantity,price,lowCount);
                productList.add(drink);
            }else if(jComboBoxStock.getSelectedItem().toString().equals("Dessert")){
                Dessert dessert = new Dessert(name,quantity,price,lowCount);
                productList.add(dessert);
            }
            txtAddProduct.setText("");
            txtAddQuantity.setText("");
            txtAddPrice.setText("");
            txtAddLowCount.setText("");
        }catch(Exception e){
        }
        
    }//GEN-LAST:event_btnAddToStockActionPerformed

    private void jComboBoxStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxStockActionPerformed
        DefaultTableModel stockTableVar = new DefaultTableModel();
        stockTableVar = (DefaultTableModel) stockTable.getModel(); 
        stockTableVar.getDataVector().removeAllElements();
        stockTableVar.fireTableDataChanged();
    }//GEN-LAST:event_jComboBoxStockActionPerformed

    @SuppressWarnings("empty-statement")
    private void btnListStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListStockActionPerformed
        try{
            DefaultTableModel stockTableVar = new DefaultTableModel();
            stockTableVar = (DefaultTableModel) stockTable.getModel(); 
            stockTableVar.getDataVector().removeAllElements();
            stockTableVar.fireTableDataChanged();
            
            if(jComboBoxStock.getSelectedItem().toString().equals("Food")){
                for(Product product : productList.getProductList()){
                    if(product instanceof Dish){
                        if(!product.isStatusValid()){
                            product.setStatus();
                            Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                            stockTableVar.addRow(dados);
                        }else{
                            Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                            stockTableVar.addRow(dados);
                        }
                    }
                }
            }else if(jComboBoxStock.getSelectedItem().toString().equals("Drink")){
                 for(Product product : productList.getProductList()){
                    if(product instanceof Drink){
                        if(!product.isStatusValid()){
                            product.setStatus();
                            Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                            stockTableVar.addRow(dados);
                        }else{
                            Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                            stockTableVar.addRow(dados);
                        }
                    }
                }
            }else if(jComboBoxStock.getSelectedItem().toString().equals("Dessert")){
                 for(Product product : productList.getProductList()){
                    if(product instanceof Dessert){
                        if(!product.isStatusValid()){
                            product.setStatus();
                            Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                            stockTableVar.addRow(dados);
                        }else{
                            Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                            stockTableVar.addRow(dados);
                        }
                    }
                }
            }
        }catch(Exception e){
        }
    }//GEN-LAST:event_btnListStockActionPerformed

    private void jComboBoxStockItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxStockItemStateChanged

    }//GEN-LAST:event_jComboBoxStockItemStateChanged

    private void btnStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStockActionPerformed
        panelMain.setVisible(false);
        panelStock.setVisible(true);
    }//GEN-LAST:event_btnStockActionPerformed

    private void btnDoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDoneActionPerformed
        panelStock.setVisible(false);
        panelMain.setVisible(true);
    }//GEN-LAST:event_btnDoneActionPerformed

    private void btnLowCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLowCountActionPerformed
        DefaultTableModel stockTableVar = new DefaultTableModel();
        stockTableVar = (DefaultTableModel) stockTable.getModel(); 
        stockTableVar.getDataVector().removeAllElements();
        stockTableVar.fireTableDataChanged();
        
        for(Product product : productList.getProductListLow()){
            if(!product.isStatusValid()){
                product.setStatus();
                Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                stockTableVar.addRow(dados);
            }
        }
    }//GEN-LAST:event_btnLowCountActionPerformed
        // TODO add your handling code here:
    private void btnConfirmNewEmployeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmNewEmployeeActionPerformed
        String name,password;
        name = txtNewEmployeeName.getText().toString();
        password = txtNewEmployeePassword.getText().toString();
        if(manager.getEmployee().getPassword().equals(txtManagerPassword.getText().toString())){
            Employee newEmployee = new Employee(name,password);
            employeeList.add(newEmployee);
            JOptionPane.showMessageDialog(panelNewEmployee, "Employee Registered!");
        }
        txtNewEmployeeName.setText("");
        txtNewEmployeePassword.setText("");
        txtManagerPassword.setText("");
    }//GEN-LAST:event_btnConfirmNewEmployeeActionPerformed

    private void txtNewTabTableNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNewTabTableNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNewTabTableNumberActionPerformed

    private void btnNewTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewTableActionPerformed
        Client newClient  = new Client(txtNewTabClientName.getText(),txtNewTabClientCpf.getText());
        Tab newTab = new Tab(newClient,employee,txtNewTabTableNumber.getText());
        tabList.add(newTab);
        jComboBoxTabs.addItem(newTab.getClient().getName());
        lblTotalPriceTab.setText("");
        
    }//GEN-LAST:event_btnNewTableActionPerformed

    private void btnRefreshTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshTableActionPerformed
            DefaultTableModel tableDishes = new DefaultTableModel();
            DefaultTableModel tableDrinks = new DefaultTableModel();
            DefaultTableModel tableDesserts = new DefaultTableModel();
            tableDishes = (DefaultTableModel) jTableDishes.getModel();
            tableDrinks = (DefaultTableModel) jTableDrinks.getModel();
            tableDesserts = (DefaultTableModel) jTableDesserts.getModel();
            
            tableDishes.getDataVector().removeAllElements();
            tableDishes.fireTableDataChanged();
            tableDrinks.getDataVector().removeAllElements();
            tableDrinks.fireTableDataChanged();
            tableDesserts.getDataVector().removeAllElements();
            tableDesserts.fireTableDataChanged();
            
            try{
                for(Product product : productList.getProductList()){
                    if(product instanceof Dish){
                        Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                        if(!product.isStatusValid()){
                            productList.remove(product);
                            productList.addLow(product);
                            JOptionPane.showMessageDialog(null, "The following item needs attention: "+ product.getName());
                        }else{
                            tableDishes.addRow(dados);
                        }
                    }
                }
                
                for(Product product : productList.getProductList()){
                    if(product instanceof Drink){
                        Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                        if(!product.isStatusValid()){
                            productList.remove(product);
                            productList.addLow(product);
                            JOptionPane.showMessageDialog(null, "The following item needs attention: "+ product.getName());
                        }else{
                            tableDrinks.addRow(dados);
                        }
                    }
                }
                
                 for(Product product : productList.getProductList()){
                    if(product instanceof Dessert){
                        Object[] dados = {product.getName(),product.getQuantity(),product.getPrice(),product.getStatus()};
                        if(!product.isStatusValid()){
                            productList.remove(product);
                            productList.addLow(product);
                            JOptionPane.showMessageDialog(null, "The following item needs attention: "+ product.getName());
                        }else{
                            tableDesserts.addRow(dados);
                        }
                    }
                }
            }catch(Exception e){
            
            }
    }//GEN-LAST:event_btnRefreshTableActionPerformed

    private void btnAddDishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDishActionPerformed
        try{
            String name,price;
            Integer quantity = 1;
            name = (jTableDishes.getValueAt(jTableDishes.getSelectedRow(), 0).toString()); 
            price = (jTableDishes.getValueAt(jTableDishes.getSelectedRow(), 2).toString());
            Object[] dados = {name,quantity.toString(),price};
            table.addRow(dados);
        }catch(Exception e){
        
        }
        
    }//GEN-LAST:event_btnAddDishActionPerformed

    private void jComboBoxTabsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxTabsActionPerformed
      printClientTab();
    }//GEN-LAST:event_jComboBoxTabsActionPerformed

    private void btnConfirmProductsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmProductsActionPerformed
       try{
        int i;
        int rows  = jTableTab.getRowCount();
            for(i=0 ; i<rows ; i++){
               String name =  jTableTab.getValueAt(i,0).toString();
               for(Product product : productList.getProductList()){
                    if(product.getName().equals(name)){
                        if(product.isStatusValid()){
                            product.Minus();
                            Product newProduct = new Product(product.getName(),1,product.getPrice(),product.getLowCount());
                            tab.add(newProduct);
                        }
                    }
                }
            }
        float total = 0;
        for(Product product : tab.getTab()){
            total = product.getPrice() + total;
        }
        lblTotalPriceTab.setText(""+total);
          
        tab.setComments(txtComments.getText().toString());
        txtComments.setText("");
        table.getDataVector().removeAllElements();
        table.fireTableDataChanged();
        printClientTab();
       }catch(Exception e){
       
       }
    }//GEN-LAST:event_btnConfirmProductsActionPerformed

    private void txtNewTabClientCpfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNewTabClientCpfActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNewTabClientCpfActionPerformed

    private void btnAddDrinksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDrinksActionPerformed
        try{
            String name,price;
            Integer quantity = 1;
            name = (jTableDrinks.getValueAt(jTableDrinks.getSelectedRow(), 0).toString());
            price = (jTableDrinks.getValueAt(jTableDrinks.getSelectedRow(), 2).toString());
            Object[] dados = {name,quantity.toString(),price};
            table.addRow(dados);
        }catch(Exception e){
        
        }
            
    }//GEN-LAST:event_btnAddDrinksActionPerformed

    private void btnAddDessertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddDessertActionPerformed
         try{
            String name,price;
            Integer quantity = 1;
            name = (jTableDesserts.getValueAt(jTableDesserts.getSelectedRow(), 0).toString());
            price = (jTableDesserts.getValueAt(jTableDesserts.getSelectedRow(), 2).toString());
            Object[] dados = {name,quantity.toString(),price};
            table.addRow(dados);
         }catch(Exception e){
         
         }
    }//GEN-LAST:event_btnAddDessertActionPerformed

    private void btnRemoveProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveProductActionPerformed
        table.removeRow(jTableTab.getSelectedRow());
    }//GEN-LAST:event_btnRemoveProductActionPerformed

    private void btnCloseTabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseTabActionPerformed
        try{
            float total = 0;
            for(Product product : tab.getTab()){
                total = product.getPrice() + total;
            }
            if(rbCredit.isSelected()){
                tab.setPaymentMethod("Credit");
                tab.setTotalPayment(total);
                tabList.addClosed(tab);
                tabList.remove(tab);
                lblTotalPriceTab.setText(""+total);
                jComboBoxTabs.removeItem(tab.getClient().getName());
            }else if(rbDebit.isSelected()){
                tab.setPaymentMethod("Debit");
                tab.setTotalPayment(total);
                tabList.addClosed(tab);
                tabList.remove(tab);
                lblTotalPriceTab.setText(""+total);
                jComboBoxTabs.removeItem(tab.getClient().getName());
            }else{
                JOptionPane.showMessageDialog(null, "You must choose a payment method.");
            }
            
        }catch(Exception e){
        }
    }//GEN-LAST:event_btnCloseTabActionPerformed

    private void btnRecordsListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecordsListActionPerformed
        DefaultTableModel tableRecords = new DefaultTableModel();
        tableRecords = (DefaultTableModel) jTableRecords.getModel();
        tableRecords.getDataVector().removeAllElements();
        tableRecords.fireTableDataChanged();
        try{
                    for(Tab tabRecord : tabList.getClosedTabList()){
                        Object[] dados = {tabRecord.getClient().getName(), tabRecord.getPaymentMethod(),tabRecord.getTotalPayment()};
                        tableRecords.addRow(dados);
                        
                    }
                
        
       }catch(Exception e){
       
       }
        
    }//GEN-LAST:event_btnRecordsListActionPerformed

    private void printClientTab(){
        try{
        DefaultTableModel tableCheckout = new DefaultTableModel();
        tableCheckout = (DefaultTableModel) jTableCheckout.getModel();
        tableCheckout.getDataVector().removeAllElements();
        tableCheckout.fireTableDataChanged();
        lblTotalPriceTab.setText("");
            for(Tab tabSearch : tabList.getTabList()){
                if(jComboBoxTabs.getSelectedItem().toString().equals(tabSearch.getClient().getName())){
                    tab = tabSearch;
                    for(Product product : tab.getTab()){
                        Object[] dados = {product.getName(), product.getQuantity(),product.getPrice()};
                        tableCheckout.addRow(dados);
                        txtCommentsTab.setText(tab.getComments());
                    }
                }
        }
       }catch(Exception e){
       
       }
    }
   
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddDessert;
    private javax.swing.JButton btnAddDish;
    private javax.swing.JButton btnAddDrinks;
    private javax.swing.JButton btnAddToStock;
    private javax.swing.JButton btnCloseTab;
    private javax.swing.JButton btnConfirmNewEmployee;
    private javax.swing.JButton btnConfirmProducts;
    private javax.swing.JButton btnDone;
    private javax.swing.JButton btnEmployeeLogout;
    private javax.swing.JButton btnListStock;
    private javax.swing.JButton btnLoginSignin;
    private javax.swing.JButton btnLowCount;
    private javax.swing.JButton btnManagerLogout;
    private javax.swing.JButton btnNewTable;
    private javax.swing.JButton btnRecordsList;
    private javax.swing.JButton btnRefreshTable;
    private javax.swing.JButton btnRemoveProduct;
    private javax.swing.JButton btnStock;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> jComboBoxStock;
    private javax.swing.JComboBox<String> jComboBoxTabs;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jTabbedDesserts;
    private javax.swing.JTabbedPane jTabbedDishes;
    private javax.swing.JTabbedPane jTabbedDrinks;
    private javax.swing.JTable jTableCheckout;
    private javax.swing.JTable jTableDesserts;
    private javax.swing.JTable jTableDishes;
    private javax.swing.JTable jTableDrinks;
    private javax.swing.JTable jTableRecords;
    private javax.swing.JTable jTableTab;
    private javax.swing.JLabel lblAddLowCount;
    private javax.swing.JLabel lblAddPrice;
    private javax.swing.JLabel lblAddProduct;
    private javax.swing.JLabel lblAddQuantity;
    private javax.swing.JLabel lblEmployeeDisplay;
    private javax.swing.JLabel lblEmployeeNameDisplay;
    private javax.swing.JLabel lblManagerDisplay;
    private javax.swing.JLabel lblManagerNameDisplay;
    private javax.swing.JLabel lblManagerPassword;
    private javax.swing.JLabel lblNewClientCpf;
    private javax.swing.JLabel lblNewClientName;
    private javax.swing.JLabel lblNewEmployeeName;
    private javax.swing.JLabel lblNewEmployeePassword;
    private javax.swing.JLabel lblNewTableNumber;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblPicture;
    private javax.swing.JLabel lblProductStock;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotalPriceTab;
    private javax.swing.JLabel lblUser;
    private javax.swing.JPanel panelLogin;
    private javax.swing.JPanel panelLoginData;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelNewEmployee;
    private javax.swing.JPanel panelNewTab;
    private javax.swing.JPanel panelStock;
    private javax.swing.JPanel panelTab;
    private javax.swing.JPanel panelTabProducts;
    private javax.swing.JRadioButton rbCredit;
    private javax.swing.JRadioButton rbDebit;
    private javax.swing.JPanel stockAddPanel;
    private javax.swing.JScrollPane stockScrollPane;
    private javax.swing.JTable stockTable;
    private javax.swing.JTextField txtAddLowCount;
    private javax.swing.JTextField txtAddPrice;
    private javax.swing.JTextField txtAddProduct;
    private javax.swing.JTextField txtAddQuantity;
    private javax.swing.JTextArea txtComments;
    private javax.swing.JTextArea txtCommentsTab;
    private javax.swing.JTextField txtLoginManagerPassword;
    private javax.swing.JTextField txtLoginManagerUser;
    private javax.swing.JTextField txtManagerPassword;
    private javax.swing.JTextField txtNewEmployeeName;
    private javax.swing.JTextField txtNewEmployeePassword;
    private javax.swing.JTextField txtNewTabClientCpf;
    private javax.swing.JTextField txtNewTabClientName;
    private javax.swing.JTextField txtNewTabTableNumber;
    // End of variables declaration//GEN-END:variables
}
